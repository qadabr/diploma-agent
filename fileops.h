#ifndef FILEOPS_H
#define FILEOPS_H

#include "common.h"

typedef struct file File;

File *FileOpen(char *name, int32 flags);
void FileClose(File *file);

long FileRead(File *file, char *buf, uint64 size);

#endif /* FILEOPS_H */
