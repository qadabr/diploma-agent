#ifndef MEMORY_H
#define MEMORY_H

#include "common.h"

void *GeneralAlloc(uint64 size);
void GeneralFree(void *pointer);

void *AtomicAlloc(uint64 size);
void AtomicFree(void *pointer);

void *VirtualAlloc(uint64 size);
void VirtualFree(void *pointer);

#endif /* MEMORY_H */
