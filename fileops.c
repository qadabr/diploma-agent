#include "fileops.h"

File *FileOpen(char *name, int32 flags)
{
	File *file;

	preempt_disable();
	file = filp_open(name, flags, 0666);
	preempt_enable();

	return file;
}

void FileClose(File *file)
{
	preempt_disable();
	filp_close(file, NULL);
	preempt_enable();
}

long FileRead(File *file, char *buf, uint64 size)
{
	long ret;
	mm_segment_t oldfs = get_fs();

	preempt_disable();
	set_fs(get_ds());
	if (file->f_op || file->f_op->read)
		ret = file->f_op->read(file, buf, size, &file->f_pos);
	else
		ret = -E_UNKNOWN;
	set_fs(oldfs);
	preempt_enable();

	return ret < 0 ? -E_RECEIVE : 0;
}
