#include "http.h"
#include "memory.h"

static const char _httpGet[] =
	"GET %s HTTP/1.1\r\n"
	"\r\n";

static const char _httpPost[] =
	"POST %s HTTP/1.1\r\n"
	"Content-Length: %lu\r\n"
	"Content-Type: application/x-www-form-urlencoded\r\n"
	"\r\n"
	"%s";

static char *FindContent(char *response, uint32 *length)
{
	static const char template[] = "\r\n\r\n";
	static const char contentLength[] = "Content-Length: ";

	char *content;
	char *lengthStr;

	if (IS_NULL(response)) {
		return NULL;
	}

	content = strstr(response, template);
	if (content) {
		content += sizeof(template) - 1;
	}

	lengthStr = strstr(response, contentLength);
	if (lengthStr && lengthStr < content) {
		lengthStr += sizeof(contentLength) - 1;
		if (sscanf(lengthStr, "%u", length) < 0) {
			*length = 0;
		}
	} else {
		*length = 0;
	}

	return content;
}

int32 HttpGet(SockAddr *sockAddr, char *request, char **response)
{
	int32 ret = 0;
	Sock *sock = NULL;
	char *fullRequest = NULL;
	char *fullResponse = NULL;
	char *content;
	uint32 contentLength;

	if (IS_NULL(response) || IS_NULL(sockAddr)) {
		return -E_INVALID;
	}

	fullResponse = VirtualAlloc(MAX_HTTP_LEN);
	if (IS_NULL(fullResponse)) {
		return -E_NOMEMORY;
	}

	fullRequest = VirtualAlloc(MAX_HTTP_LEN);
	if (IS_NULL(fullRequest)) {
		ret = -E_NOMEMORY;
		goto http_get_free;
	}

	snprintf(fullRequest, MAX_HTTP_LEN, _httpGet, request);

	sock = SockCreate(AF_INET, SOCK_STREAM, 0);
	if (IS_NULL(sock)) {
		ret = -E_CREATE;
		goto http_get_free;
	}

	ret = SockConnect(sock, sockAddr, sizeof(struct sockaddr_in));
	if (ret < 0) {
		LogError("Failed to connect %d\n", ret);
		goto http_get_free;
	}

	ret = SockSend(sock, fullRequest, MAX_HTTP_LEN, 0);
	if (ret < 0) {
		goto http_get_free;
	}

	ret = SockRecv(sock, fullResponse, MAX_HTTP_LEN, 0);
	if (ret < 0) {
		goto http_get_free;
	}

	content = FindContent(fullResponse, &contentLength);
	if (IS_NULL(content) || contentLength == 0) {
		LogError("response: %s", fullResponse);
		ret = -E_INVALID;
	} else {
		*response = VirtualAlloc(contentLength + 1);
		if (*response) {
			memcpy(*response, content, contentLength);
			(*response)[contentLength] = 0;
		}
	}

 http_get_free:
	if (sock)
		SockDestroy(&sock);
	if (fullResponse)
		VirtualFree(fullResponse);
	if (fullRequest)
		VirtualFree(fullRequest);

	return ret;
}

int32 HttpPost(SockAddr *sockAddr, char *route, char *message, char **response)
{
	int32 ret;
	char *fullPost = NULL;
	char *fullResponse = NULL;
	Sock *sock = NULL;
	char *content = NULL;
	uint32 contentLength;

	if (IS_NULL(response) || IS_NULL(sockAddr)) {
		return -E_INVALID;
	}

	fullPost = VirtualAlloc(MAX_HTTP_LEN);
	if (IS_NULL(fullPost)) {
		return -E_NOMEMORY;
	}

	*response = NULL;
	/* FIX ME (message might be not zero-terminated) */
	snprintf(fullPost, MAX_HTTP_LEN, _httpPost,
		 route, strlen(message), message);

	sock = SockCreate(AF_INET, SOCK_STREAM, 0);
	if (IS_NULL(sock)) {
		ret = -E_CREATE;
		goto http_post_free;
	}

	ret = SockConnect(sock, sockAddr, sizeof(struct sockaddr_in));
	if (ret < 0) {
		LogError("Failed to connect %d\n", ret);
		goto http_post_free;
	}

	ret = SockSend(sock, fullPost, MAX_HTTP_LEN, 0);
	if (ret < 0) {
		goto http_post_free;
	}

	fullResponse = VirtualAlloc(MAX_HTTP_LEN);
	if (IS_NULL(fullResponse)) {
		ret = -E_NOMEMORY;
		goto http_post_free;
	}

	ret = SockRecv(sock, fullResponse, MAX_HTTP_LEN, 0);
	if (ret < 0) {
		goto http_post_free;
	}

	content = FindContent(fullResponse, &contentLength);
	if (IS_NULL(content) || contentLength == 0) {
		LogError("response: %s", fullResponse);
		ret = -E_INVALID;
	} else {
		*response = VirtualAlloc(contentLength + 1);
		if (*response) {
			memcpy(*response, content, contentLength);
			(*response)[contentLength] = 0;
		}
	}

 http_post_free:
	if (sock)
		SockDestroy(&sock);
	if (fullResponse)
		VirtualFree(fullResponse);
	if (fullPost)
		VirtualFree(fullPost);

	return 0;
}
