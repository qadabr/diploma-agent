#include "memory.h"

void *GeneralAlloc(uint64 size)
{
	return kmalloc(size, GFP_KERNEL);
}

void GeneralFree(void *pointer)
{
	kfree(pointer);
}

void *AtomicAlloc(uint64 size)
{
	return kmalloc(size, GFP_ATOMIC);
}

void AtomicFree(void *pointer)
{
	kfree(pointer);
}

void *VirtualAlloc(uint64 size)
{
	return vmalloc(size);
}

void VirtualFree(void *pointer)
{
	vfree(pointer);
}
