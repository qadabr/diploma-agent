#ifndef THREADS_H
#define THREADS_H

#include "common.h"

struct Thread {
	struct task_struct *kthread;
	wait_queue_head_t wait;
	int32 (*func)(struct Thread *thread);
	void *args;
	atomic_t started;
	int32 joined;
	int32 ret;
};

typedef struct Thread Thread;

#define ThreadCreate(func, args) _ThreadCreate(func, args, #func)
Thread *_ThreadCreate(int32 (*func)(Thread *thread), void *args, char *name);
int32 ThreadJoin(Thread *thread);
void ThreadDestroy(Thread **thread);

void PauseCurrentThread(uint32 timeout);

#endif /* THREADS_H */
