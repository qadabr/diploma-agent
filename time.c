#include "time.h"

uint64 Timestamp(void)
{
	struct timespec t = current_kernel_time();
	return (uint64)t.tv_sec * 1000 +
	       (uint64)t.tv_nsec / 1000000;
}
