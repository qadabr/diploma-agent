#ifndef QUEUE_H
#define QUEUE_H

#include "common.h"

#define EntryRecord(address, type, field) \
	((type*)((uint8*)address - (uint8*)(&((type*)0)->field)))

typedef struct QueueEntry {
	struct QueueEntry *next;
} QueueEntry;

typedef struct QueueHead {
	QueueEntry *volatile head;
	QueueEntry *volatile tail;
	QueueEntry dummy;
} QueueHead;

static inline void QueueHeadInit(QueueHead *head)
{
	head->head = &head->dummy;
	head->tail = &head->dummy;
	head->dummy.next = (QueueEntry *)head;
}

static inline void QueueHeadDeinit(QueueHead *head)
{
	head->head = NULL;
	head->tail = NULL;
}

static inline boolean QueueIsEmpty(QueueHead *head)
{
	return head->head == head->tail;
}

static inline void *_QueueExchange(QueueEntry *volatile *dst, void *comp, void *new_val)
{
#if defined(__LP64__) || defined(_LP64)
	void *__ret = 0;
	void *__comp = comp;
	void *__new = new_val;

	__asm__ __volatile__(LOCK_PREFIX
			     "cmpxchgq %2,%1;"
			     : "=a" (__ret), "+m" (*dst)
			     : "r" (__new), "0" (__comp)
			     : "memory");
	return __ret;
#else
#error "Not implemented!"
#endif
}

static inline void QueuePush(QueueHead *head, QueueEntry *entry)
{
	QueueEntry *tail;
	QueueEntry *next;

	entry->next = (QueueEntry *)head;
	while (TRUE) {
		tail = head->tail;
		next = tail->next;

		if (next == (QueueEntry *)head) {
			/* Пытаемся добавить указатель на следующий элемент */
			if (_QueueExchange(&tail->next, head, entry) == head) {
				/*Обновляем хвост очереди*/
				_QueueExchange(&head->tail, tail, entry);
				break;
			}
		} else {
			/**
			 * Если какой-то другой поток успел изменить tail->next,
			 * меняем текущий tail на следующий
			 */
			_QueueExchange(&head->tail, tail, next);
		}
	}

	return;
}

static inline QueueEntry *QueuePop(QueueHead *head)
{
	QueueEntry *next;
	QueueEntry *old_tail;
	QueueEntry *old_head;

	while (TRUE) {
		old_head = head->head;
		old_tail = head->tail;
		next = old_head->next;

		if (head->head == old_head) {
			/* Очередь пуста */
			if (old_head == old_tail) {
				if (next == (QueueEntry *)head) {
					return NULL;
				}

				_QueueExchange(&head->tail, old_tail, next);
				continue;
			}

			/* Пытаемся извлечь элемент */
			if (_QueueExchange(&head->head, old_head, next) == old_head) {
					/* Если это не последний элемент */
				if (old_head != &head->dummy) {
					return old_head;
				}

				/**
				 * Если извлекли последний элемент,
				 * добавляем его обратно в очередь.
				 * Так как в очереди всегда должен быть хотя бы один элемент
				 */
				QueuePush(head, old_head);
			}
		}
	}

	return NULL;
}


#endif /* QUEUE_H */
