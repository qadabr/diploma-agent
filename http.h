#ifndef HTTP_H
#define HTTP_H

#include "common.h"
#include "sock.h"

#define MAX_HTTP_LEN (8196 * 64)
#define DATA_CHUNK_SIZE (MAX_HTTP_LEN)

int32 HttpGet(SockAddr *sockAddr, char *request, char **response);
int32 HttpPost(SockAddr *sockAddr, char *route, char *message, char **response);

#endif /* HTTP_H */
