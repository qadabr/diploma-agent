#ifndef DATASOURCE_H
#define DATASOURCE_H

#include "common.h"

int32 DatasourceInit(uint32 address, uint16 port);
void DatasourceExit(void);

void DatasourcePush(char *route, char *request);

#endif /* DATASOURCE_H */
