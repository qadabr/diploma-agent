#ifndef TIME_H
#define TIME_H

#include "common.h"

uint64 Timestamp(void);

#endif /* TIME_H */
