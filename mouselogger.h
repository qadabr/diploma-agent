#ifndef MOUSELOGGER_H
#define MOUSELOGGER_H

#include "common.h"
#include "queue.h"

typedef struct MouseloggerEvent {
	char x;
	char y;
	int32 l;
	int32 r;
	int32 m;
	uint64 timestamp;
} MouseloggerEvent;

typedef struct MouseloggerEventEntry {
	QueueEntry entry;
	MouseloggerEvent event;
} MouseloggerEventEntry;

int32 MouseloggerInit(void);
int32 MouseloggerExit(void);

#endif /* MOUSELOGGER_H */
