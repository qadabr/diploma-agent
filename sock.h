#ifndef SOCK_H
#define SOCK_H

#include "common.h"

typedef struct socket Sock;
typedef struct sockaddr SockAddr;

Sock *SockCreate(int32 domain, int32 type, int32 proto);
void SockDestroy(Sock **sock);
int32 SockConnect(Sock *sock, SockAddr *name, int32 nameLen);
int32 SockSend(Sock *sock, char *buf, word len, int32 flags);
int32 SockRecv(Sock *sock, char *buf, word len, int32 flags);
int32 SockShutdown(Sock *sock, int32 how);
void SockClose(Sock *sock);

#endif /* SOCK_H */
