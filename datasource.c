#include "datasource.h"
#include "threads.h"
#include "memory.h"
#include "queue.h"
#include "http.h"
#include "sock.h"
#include "hash.h"

#define DEFAULT_PAUSE (5000)
#define DATASOURCE_ROUTE "/"

typedef struct DatasourceEntry {
	QueueEntry entry;
	char *route;
	char *message;
} DatasourceEntry;

static Thread *_datasourceWorker = NULL;
static boolean _datasourceRunning;

static QueueHead _requestQueue;

static inline DatasourceEntry *DatasourceEntryCreate(char *route, char *message)
{
	DatasourceEntry *entry;

	entry = GeneralAlloc(sizeof(*entry));
	if (IS_NULL(entry))
		return NULL;

	entry->route = VirtualAlloc(strlen(route) + 1);
	if (IS_NULL(entry->route)) {
		GeneralFree(entry);
		return NULL;
	}

	entry->message = VirtualAlloc(strlen(message) + 1);
	if (IS_NULL(entry->message)) {
		VirtualFree(entry->message);
		GeneralFree(entry);
		return NULL;
	}

	strcpy(entry->route, route);
	strcpy(entry->message, message);

	return entry;
}

static inline void DatasourceEntryDestroy(DatasourceEntry *entry)
{
	if (entry) {
		if (entry->route)
			VirtualFree(entry->route);
		if (entry->message)
			VirtualFree(entry->message);

		GeneralFree(entry);
	}
}

void DatasourcePush(char *route, char *request)
{
	DatasourceEntry *entry;
	char *fullRoute;

	fullRoute = VirtualAlloc(1024);
	if (IS_NULL(fullRoute)) {
		return;
	}

	sprintf(fullRoute, route, HashId());

	entry = DatasourceEntryCreate(fullRoute, request);
	if (entry) {
		QueuePush(&_requestQueue, &entry->entry);
	}

	VirtualFree(fullRoute);
}

static char *GenerateRequest(void)
{
	char *response;

	response = VirtualAlloc(2048);
	if (IS_NULL(response)) {
		return NULL;
	}

	snprintf(response, 2048,
		 "/agent_update/%016llx/%s",
		 HashId(), utsname()->nodename);

	return response;
}

static int32 DatasourceWorker(Thread *thread)
{
	int ret = 0;
	_datasourceRunning = TRUE;

	for (; _datasourceRunning; PauseCurrentThread(DEFAULT_PAUSE)) {
		QueueEntry *qe;
		char *response;
		char *request;

		request = GenerateRequest();
		if (request) {
			ret = HttpGet((SockAddr *)thread->args, request, &response);
			if (ret < 0) {
				LogError("Failed to http get request %d\n", ret);
			} else {
				LogDebug("%s", response);
				VirtualFree(response);
			}

			VirtualFree(request);
		} else {
			continue;
		}

		while ((qe = QueuePop(&_requestQueue)) != NULL) {
			DatasourceEntry *requestEntry;
			char *response;

			requestEntry = EntryRecord(qe, DatasourceEntry, entry);

			response = VirtualAlloc(MAX_HTTP_LEN);
			if (response) {
				ret = HttpPost((SockAddr *)thread->args,
					       requestEntry->route,
					       requestEntry->message,
					       &response);

				if (IS_ERROR(ret)) {
					LogError("Error while communicate: %d\n", ret);
					/* Возвращаем запрос в очередь */
					QueuePush(&_requestQueue, &requestEntry->entry);
					break;
				} else {
					LogDebug("Post '%s' '%s'\n", requestEntry->route, response);
					/* Если же всё норм, то уничтожаем инфу о запросе */
					DatasourceEntryDestroy(requestEntry);
				}

				VirtualFree(response);
			}

			PauseCurrentThread(1000);
		}
	}

	GeneralFree(thread->args);
	return ret;
}

int32 DatasourceInit(uint32 address, uint16 port)
{
	struct sockaddr_in *addr = (struct sockaddr_in *)GeneralAlloc(sizeof(*addr));
	if (IS_NULL(addr)) {
		return -E_NOMEMORY;
	}

	QueueHeadInit(&_requestQueue);

	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = address;
	addr->sin_port = htons(port);

	_datasourceWorker = ThreadCreate(DatasourceWorker, addr);
	if (IS_NULL(_datasourceWorker)) {
		GeneralFree(addr);
		return -E_CREATE;
	}

	return 0;
}

void DatasourceExit(void)
{
	_datasourceRunning = FALSE;

	ThreadJoin(_datasourceWorker);
	ThreadDestroy(&_datasourceWorker);

	QueueHeadDeinit(&_requestQueue);
}
