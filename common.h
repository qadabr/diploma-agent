#ifndef COMMON_H
#define COMMON_H

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/kmod.h>
#include <linux/kallsyms.h>
#include <linux/limits.h>
#include <linux/slab.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/udp.h>
#include <linux/ip.h>
#include <linux/keyboard.h>
#include <linux/atomic.h>
#include <linux/kthread.h>
#include <linux/notifier.h>
#include <linux/utsname.h>
#include <linux/version.h>
#include <linux/vmalloc.h>
#include <asm/errno.h>
#include <asm/unistd.h>
#include <asm/uaccess.h>
#include <net/inet_sock.h>

#define MOD_INIT static int __init
#define MOD_EXIT static void __exit

#define _LOG_PRINT_DEBUG(file, line, func, fmt, ...) printk(KERN_DEBUG "[DEBUG] %s:%u (%s) " fmt, file, line, func, ##__VA_ARGS__)
#define _LOG_PRINT_ERROR(file, line, func, fmt, ...) printk(KERN_ERR   "[ERROR] %s:%u (%s) " fmt, file, line, func, ##__VA_ARGS__)

#define LogDebug(fmt, ...) _LOG_PRINT_DEBUG(__FILE__, __LINE__, __FUNCTION__, fmt, ##__VA_ARGS__)
#define LogError(fmt, ...) _LOG_PRINT_ERROR(__FILE__, __LINE__, __FUNCTION__, fmt, ##__VA_ARGS__)

typedef s8	int8;
typedef s16	int16;
typedef s32	int32;
typedef s64	int64;

typedef u8	uint8;
typedef u16	uint16;
typedef u32	uint32;
typedef u64	uint64;

typedef size_t	word;
typedef int	boolean;

#define TRUE	1
#define FALSE	0

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Vladislav Belousov");
MODULE_DESCRIPTION("User Behaviour Analytics");

#define IS_NULL(x) ((x) == NULL)
#define IS_ERROR(x) ((int32)(x) < 0 && (int32)(x) >= -E_ERRORMAX)

#define E_UNKNOWN       1   /* Unsuccessful */
#define E_NOMEMORY      2   /* No memory */
#define E_NOSPACE       3   /* No space */
#define E_ALREADY       4   /* The action has already occured */
#define E_INVALID       5   /* Invalid argument */
#define E_CREATE        6   /* Socket creation error */
#define E_CONNECT       7   /* No connection */
#define E_SEND          8   /* Sending error */
#define E_RECEIVE       9   /* Receiving error */
#define E_OPEN          10  /* Open error */
#define E_ERRORMAX      11

uint64 HashDjb2(char *s);

static inline uint64 HashId(void)
{
	static uint64 hash = 0;

	if (hash == 0)
		hash = HashDjb2(utsname()->nodename) ^ HashDjb2(utsname()->version);

	return hash;
}

#endif /* COMMON_H */
