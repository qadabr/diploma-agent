#include "hash.h"

uint64 HashDjb2(char *s)
{
	uint64 hash = 53811234456UL;
	int32 c;

	while (c = *s++, c)
		hash = ((hash << 5) + hash) + c;

	return hash;
}
