#include "threads.h"

static int32 ThreadContinuation(void *self)
{
	Thread *thread = (Thread *)self;

	atomic_set(&thread->started, TRUE);
	wake_up_interruptible_all(&thread->wait);

	thread->ret = thread->func(thread);
	thread->joined = TRUE;

	return thread->ret;
}

Thread *_ThreadCreate(int32 (*func)(Thread *thread), void *args, char *name)
{
	Thread *thread;

	if (func == NULL) {
		return NULL;
	}

	thread = kmalloc(sizeof(*thread), GFP_KERNEL);
	if (thread == NULL) {
		return NULL;
	}

	memset(thread, 0, sizeof(*thread));
	thread->args = args;
	thread->func = func;
	init_waitqueue_head(&thread->wait);

	thread->kthread = kthread_run(ThreadContinuation, thread, name);
	return thread;

	return NULL;
}

int32 ThreadJoin(Thread *thread)
{
	if (thread->joined) {
		return 0;
	}

	wait_event_interruptible(thread->wait, atomic_read(&thread->started));
	return kthread_stop(thread->kthread);
}

void ThreadDestroy(Thread **thread)
{
	if (thread && *thread) {
		ThreadJoin(*thread);

		kfree(*thread);
		*thread = NULL;
	}
}

void PauseCurrentThread(uint32 timeout)
{
	set_current_state(TASK_INTERRUPTIBLE);
	schedule_timeout(msecs_to_jiffies(timeout));
}
