#include "sock.h"
#include "memory.h"

Sock *SockCreate(int32 domain, int32 type, int32 proto)
{
	int32 err;
	Sock *sock;

	sock = AtomicAlloc(sizeof(*sock));
	if (sock == NULL) {
		return NULL;
	}

	err = sock_create(domain, type, proto, &sock);
	if (err != 0) {
		AtomicFree(sock);
		return NULL;
	}

	return sock;
}

void SockDestroy(Sock **sock)
{
	if (sock && *sock) {
		SockClose(*sock);
		AtomicFree(*sock);
		*sock = NULL;
	}
}

int32 SockConnect(Sock *sock, SockAddr *addr, int32 addrLen)
{
	return kernel_connect(sock, addr, addrLen, 0) < 0 ? -E_CONNECT : 0;
}

int32 SockSend(Sock *sock, char *buf, word len, int32 flags)
{
	int32 ret;
	struct msghdr msg;
	struct kvec vec;

	memset(&msg, 0, sizeof(msg));
	msg.msg_flags = flags;
	vec.iov_base = buf;
	vec.iov_len = (__kernel_size_t)len;

	ret = kernel_sendmsg(sock, &msg, &vec, 1, len);
	return ret < 0 ? -E_SEND : ret;
}

int32 SockRecv(Sock *sock, char *buf, word len, int32 flags)
{
	int32 ret;
	struct msghdr msg;
	struct kvec vec;

	memset(&msg, 0, sizeof(msg));
	vec.iov_base = buf;
	vec.iov_len = (__kernel_size_t)len;

	ret = kernel_recvmsg(sock, &msg, &vec, 1, len, flags);
	return ret < 0 ? -E_RECEIVE : ret;
}

int32 SockShutdown(Sock *sock, int32 how)
{
	return kernel_sock_shutdown(sock, how) < 0 ? -E_UNKNOWN : 0;
}

void SockClose(Sock *sock)
{
	if (sock->ops && sock->ops->release) {
		sock->ops->release(sock);
	}
}
